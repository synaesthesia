/* Synaesthesia - program to display sound graphically
   Raw output module.
   Copyright (C) 2009  Rafał Rzepecki <divided.mind@gmail.com>

  This program is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the
  Free Software Foundation; either version 2 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>
#include "syna.h"

static uint32_t raw_palette[256];
static uint32_t *fbuf;
static unsigned int framecount = 0, dropped = 0;
static struct timeval frametime;
static struct timeval framelen;

bool RawScreen::init(int xHint,int yHint,int widthHint,int heightHint,bool fullscreen) {
  fprintf(stderr, "Using raw video output, size %dx%d, frame size %d bytes.\n", widthHint, heightHint, widthHint * heightHint * 3);
  outWidth = widthHint;
  outHeight = heightHint;
  fbuf = (uint32_t *)malloc(widthHint * heightHint * 4);

  gettimeofday(&frametime, 0);
  framelen.tv_sec = 0;
  framelen.tv_usec = 1000000/fps;

  return true;
}

void RawScreen::setPalette(unsigned char *palette) {
  for(int i=0;i<256;i++) {
    raw_palette[i] = palette[i*3+0]
      | palette[i*3+1] << 8
      | palette[i*3+2] << 16;
  }
}

void RawScreen::end() {
  fprintf(stderr, "%d frames, %d dropped\n", framecount, dropped);
}

void RawScreen::inputUpdate(int &mouseX,int &mouseY,int &mouseButtons,char &keyHit) {
  keyHit = 0;
}

void RawScreen::show(void) {
  register uint32_t *ptr2 = (uint32_t*)output;
  register uint32_t *ptr1 = fbuf;
  int i = outHeight * outWidth/sizeof(uint32_t);
  // Asger Alstrup Nielsen's (alstrup@diku.dk)
  // optimized 32 bit screen loop
  do {
    //Original bytewize version:
    //unsigned char v = (*(ptr2++)&15*16);
    //*(ptr1++) = v|(*(ptr2++)>>4);
    register uint32_t const r1 = *(ptr2++);
    register uint32_t const r2 = *(ptr2++);

    //Fade will continue even after value > 16
    //thus black pixel will be written when values just > 0
    //thus no need to write true black
    //if (r1 || r2) {
#ifdef LITTLEENDIAN
      register uint32_t const v =
          ((r1 & 0x000000f0ul) >> 4)
        | ((r1 & 0x0000f000ul) >> 8)
        | ((r1 & 0x00f00000ul) >> 12)
        | ((r1 & 0xf0000000ul) >> 16);
      register uint32_t const p = v |
        ( ((r2 & 0x000000f0ul) << 12)
        | ((r2 & 0x0000f000ul) << 8)
        | ((r2 & 0x00f00000ul) << 4)
        | ((r2 & 0xf0000000ul)));
      const unsigned char *o = reinterpret_cast<const unsigned char *>(&p);
      *(ptr1++) = raw_palette[o[0]];
      *(ptr1++) = raw_palette[o[1]];
      *(ptr1++) = raw_palette[o[2]];
      *(ptr1++) = raw_palette[o[3]];
#else
      register uint32_t const v =
          ((r2 & 0x000000f0ul) >> 4)
        | ((r2 & 0x0000f000ul) >> 8)
        | ((r2 & 0x00f00000ul) >> 12)
        | ((r2 & 0xf0000000ul) >> 16);
      register uint32_t const p = v |
        ( ((r1 & 0x000000f0ul) << 12)
        | ((r1 & 0x0000f000ul) << 8)
        | ((r1 & 0x00f00000ul) << 4)
        | ((r1 & 0xf0000000ul)));
      const unsigned char *o = reinterpret_cast<const unsigned char *>(&p);
      *(ptr1++) = raw_palette[o[0]];
      *(ptr1++) = raw_palette[o[1]];
      *(ptr1++) = raw_palette[o[2]];
      *(ptr1++) = raw_palette[o[3]];
#endif
    //} else {
    //  ptr1++;
    //}
  } while (--i);

  static struct timespec ts = {0, 10000};
  struct timeval tod;
  gettimeofday(&tod, 0);

  while (timercmp(&tod, &frametime, <)) {
    gettimeofday(&tod, 0);
    nanosleep(&ts, 0);
  }

  int drop = 0;
  while(timercmp(&tod, &frametime, >)) {
    framecount++;
    write(1, fbuf, outWidth * outHeight * 4);
    timeradd(&frametime, &framelen, &frametime);
    if (drop++) {
      dropped++;
    }
  }
}
